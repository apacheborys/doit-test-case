<?php
namespace Controller;

abstract class AbstractController
{
    public function showJson(array $data)
    {
        echo json_encode($data);
    }
}