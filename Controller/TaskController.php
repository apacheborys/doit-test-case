<?php
namespace Controller;

use Entity\Request\GetTaskEntity;
use Entity\TaskEntity;
use Repository\TaskRepository;

class TaskController extends AbstractController
{
    public function getTaskAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        $rawFilter = new GetTaskEntity();
        $requestBody['filter']['email'] = getenv("USER");
        $rawFilter->fillFromArray($requestBody);
        $rawFilter->validate();

        if (count($rawFilter->validationErrors) > 0) {
            $this->showJson(array_merge(['Something happen wrong'], $rawFilter->validationErrors));
            return;
        }

        $repoTask = new TaskRepository();
        $tasksFromDb = $repoTask->findBy(
            $rawFilter->getFilters()->toArray(),
            $rawFilter->getLimit(),
            $rawFilter->getOffset(),
            $rawFilter->getOrderBy(),
            $rawFilter->getOrderDirection()
        );

        $responseArray = [];

        foreach ($tasksFromDb as $taskFromDb) {
            $responseArray[] = $taskFromDb->toArray();
        }

        $this->showJson($responseArray);
        return;
    }

    public function createTaskAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        $rawTask = new TaskEntity();
        $rawTask->setEmail(getenv("USER"));
        $rawTask->setDone(false);
        $rawTask->fillFromArray($requestBody);
        $rawTask->validate();

        if (count($rawTask->validationErrors) > 0) {
            $this->showJson(array_merge(['Something happen wrong'], $rawTask->validationErrors));
            return;
        }

        $repoTask = new TaskRepository();
        if (!$repoTask->insert($rawTask)) {
            $this->showJson(['Can\'t insert new task to db', $repoTask->getLastError()]);
            return;
        }

        $this->showJson($rawTask->toArray());
        return;
    }

    public function updateTaskAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        if (!isset($requestBody['title'])) {
            $this->showJson(['Didn\'t find title element for properly fetch task from db']);
            return;
        }

        $repoTask = new TaskRepository();

        $taskFromDb = $repoTask->findBy(['email' => getenv("USER"), 'title' => $requestBody['title']]);

        if (!$taskFromDb) {
            $this->showJson(['Can\'t find task']);
            return;
        }

        $taskFromDb = $taskFromDb[0];
        $taskFromDb->fillFromArray($requestBody);
        $taskFromDb->validate();

        if (count($taskFromDb->validationErrors) > 0) {
            $this->showJson(array_merge(['Something happen wrong'], $taskFromDb->validationErrors));
            return;
        }

        if (!$repoTask->update($taskFromDb)) {
            $this->showJson(['Can\'t update this task in db', $repoTask->getLastError()]);
            return;
        }

        $this->showJson($taskFromDb->toArray());
        return;
    }

    public function deleteTaskAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        if (!isset($requestBody['title'])) {
            $this->showJson(['Didn\'t find title element for properly fetch task from db']);
            return;
        }

        $repoTask = new TaskRepository();

        $taskFromDb = $repoTask->findBy(['email' => getenv("USER"), 'title' => $requestBody['title']]);

        if (!$taskFromDb) {
            $this->showJson(['Can\'t find task']);
            return;
        }

        if (!$repoTask->delete($taskFromDb[0])) {
            $this->showJson(['Can\'t delete this task from db', $repoTask->getLastError()]);
            return;
        }

        $this->showJson(['success' => true]);
        return;
    }
}