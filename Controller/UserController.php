<?php
namespace Controller;

use Entity\TokenEntity;
use Entity\UserEntity;
use Repository\TokenRepository;
use Repository\UserRepository;

class UserController extends AbstractController
{
    /**
     * Sign up action
     */
    public function signUpAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        $rawUser = new UserEntity();
        $rawUser->fillFromArray($requestBody);
        $rawUser->validate();

        if (count($rawUser->validationErrors) > 0) {
            $this->showJson(array_merge(['Something happen wrong'], $rawUser->validationErrors));
            return;
        }

        $repoUser = new UserRepository();

        if (!$repoUser->insert($rawUser)) {
            $this->showJson(['Can\'t insert new user to db', $repoUser->getLastError()]);
            return;
        }

        try {
            $originalToken = bin2hex(random_bytes(16));
        } catch (\Exception $e) {
            $this->showJson(['Can\'t generate new token', $e->getMessage(), $e->getTraceAsString()]);
            return;
        }

        $newToken = new TokenEntity();
        $newToken->setEmail($rawUser->getEmail());
        $newToken->setToken($originalToken);

        $repoToken = new TokenRepository();

        if (!$repoToken->insert($newToken)) {
            $this->showJson(['Can\'t insert new token to db', $repoToken->getLastError()]);
            return;
        }

        $this->showJson(['token' => $newToken->getToken()]);
    }

    /**
     * Sign in action
     */
    public function signInAction()
    {
        $requestBody = json_decode(file_get_contents('php://input'), true);

        $rawUser = new UserEntity();
        $rawUser->fillFromArray($requestBody);
        $rawUser->validate();

        if (count($rawUser->validationErrors) > 0) {
            $this->showJson(array_merge(['Something happen wrong'], $rawUser->validationErrors));
            return;
        }

        $repoUser = new UserRepository();

        if (!$repoUser->find($rawUser)) {
            $this->showJson(['Can\'t find user with provided login and password']);
            return;
        }

        try {
            $originalToken = bin2hex(random_bytes(16));
        } catch (\Exception $e) {
            $this->showJson(['Can\'t generate new token', $e->getMessage(), $e->getTraceAsString()]);
            return;
        }

        $repoToken = new TokenRepository();

        if ($tokenFromDb = $repoToken->findBy(['email' => $rawUser->getEmail()])) {
            $tokenFromDb = $tokenFromDb[0];

            $tokenFromDb->setToken($originalToken);

            if (!$repoToken->update($tokenFromDb)) {
                $this->showJson(['Can\'t update token in db', $repoToken->getLastError()]);
                return;
            }
        } else {
            $newToken = new TokenEntity();
            $newToken->setEmail($rawUser->getEmail());
            $newToken->setToken($originalToken);

            if (!$repoToken->insert($newToken)) {
                $this->showJson(['Can\'t insert new token to db', $repoToken->getLastError()]);
                return;
            }
        }

        $this->showJson(['token' => $originalToken]);
    }
}