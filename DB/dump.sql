CREATE TABLE IF NOT EXISTS `user` (
    email VARCHAR(256) PRIMARY KEY,
    password VARCHAR (256) NOT NULL
);

CREATE TABLE IF NOT EXISTS `token` (
    email VARCHAR (256) PRIMARY KEY,
    token VARCHAR (256) NOT NULL
);

BEGIN;
CREATE TABLE IF NOT EXISTS `task` (
    email VARCHAR(256) NOT NULL,
    title VARCHAR (256) NOT NULL,
    dueDate DATETIME NOT NULL,
    priority VARCHAR (8) CHECK( priority IN ('Low','Normal','High') ),
    done INTEGER (1) DEFAULT 0
);
CREATE UNIQUE INDEX IF NOT EXISTS task_key_idx ON task (email, title);
CREATE INDEX IF NOT EXISTS task_title_idx ON task (title);
CREATE INDEX IF NOT EXISTS task_dueDate_idx ON task (dueDate);
CREATE INDEX IF NOT EXISTS task_priority_idx ON task (priority);
CREATE INDEX IF NOT EXISTS task_done_idx ON task (done);
COMMIT;