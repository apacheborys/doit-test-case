<?php
namespace Entity\Request;

use Entity\Request;

class GetTaskEntity
{
    /**
     * @var string $orderBy
     */
    private $orderBy;

    /**
     * @var string $orderDirection
     */
    private $orderDirection;

    /**
     * @var integer $limit
     */
    private $limit;

    /**
     * @var integer $offset
     */
    private $offset;

    /**
     * @var GetTaskFilterEntity
     */
    private $filters;

    /**
     * @var string[] $validationErrors
     */
    public $validationErrors = [];

    /**
     * @return string
     */
    public function getOrderBy(): string
    {
        return $this->orderBy;
    }

    /**
     * @param string $orderBy
     */
    public function setOrderBy(string $orderBy): void
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return string
     */
    public function getOrderDirection(): string
    {
        return $this->orderDirection;
    }

    /**
     * @param string $orderDirection
     */
    public function setOrderDirection(string $orderDirection): void
    {
        $this->orderDirection = $orderDirection;
    }

    /**
     * @return int
     */
    public function getLimit(): int
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset(int $offset): void
    {
        $this->offset = $offset;
    }

    /**
     * @return GetTaskFilterEntity
     */
    public function getFilters(): GetTaskFilterEntity
    {
        return $this->filters;
    }

    /**
     * @param GetTaskFilterEntity $filters
     */
    public function setFilters(GetTaskFilterEntity $filters): void
    {
        $this->filters = $filters;
    }

    public function fillFromArray($data)
    {
        if (isset($data['orderBy'])) {
            $this->orderBy = $data['orderBy'];
        }

        if (isset($data['orderDirection'])) {
            $this->orderDirection = $data['orderDirection'];
        }

        if (isset($data['limit'])) {
            $this->limit = $data['limit'];
        }

        if (isset($data['offset'])) {
            $this->offset = $data['offset'];
        }

        if (isset($data['filters'])) {
            $this->filters = new GetTaskFilterEntity();
            $this->filters->fillFromArray($data['filters']);
        }
    }

    /**
     * Validation fields
     */
    public function validate()
    {
        if (!in_array($this->orderBy, ['title', 'priority', 'dueDate', 'done'])) {
            $this->validationErrors[] = 'Order can be only title, priority, dueDate or done';
        }

        if (!in_array($this->orderDirection, ['ASC', 'DESC'])) {
            $this->validationErrors[] = 'Order direction can be only ASC or DESC';
        }

        if (!is_int($this->limit)) {
            $this->validationErrors[] = 'Limit can be only integer';
        }

        if (!is_int($this->offset)) {
            $this->validationErrors[] = 'Offset can be only integer';
        }

        array_merge($this->validationErrors, $this->filters->validationErrors);
    }
}