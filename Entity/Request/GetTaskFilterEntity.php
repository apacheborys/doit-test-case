<?php
namespace Entity\Request;

use Entity\TaskEntity;

class GetTaskFilterEntity extends TaskEntity
{
    public function validate()
    {
        if ($this->priority && !in_array($this->priority, ['Low', 'Normal', 'High'])) {
            $this->validationErrors[] = 'Priority can be only Low, Normal or High';
        }

        if ($this->title && strlen($this->title) > 256) {
            $this->validationErrors[] = 'Title can\'t be more than 256 characters';
        }
    }
}