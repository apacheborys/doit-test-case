<?php
namespace Entity;

class TaskEntity
{
    /**
     * @var string $email
     */
    protected $email;

    /**
     * @var string $title
     */
    protected $title;

    /**
     * @var \DateTime $dueDate
     */
    protected $dueDate;

    /**
     * @var string $priority
     */
    protected $priority;

    /**
     * @var bool $done
     */
    protected $done;

    /**
     * @var string[] $validationErrors
     */
    public $validationErrors = [];

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return \DateTime
     */
    public function getDueDate(): \DateTime
    {
        return $this->dueDate;
    }

    /**
     * @param \DateTime $dueDate
     */
    public function setDueDate(\DateTime $dueDate): void
    {
        $this->dueDate = $dueDate;
    }

    /**
     * @return string
     */
    public function getPriority(): string
    {
        return $this->priority;
    }

    /**
     * @param string $priority
     */
    public function setPriority(string $priority): void
    {
        $this->priority = $priority;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->done;
    }

    /**
     * @param bool $done
     */
    public function setDone(bool $done): void
    {
        $this->done = $done;
    }

    public function fillFromArray($data)
    {
        if (isset($data['title'])) {
            $this->title = $data['title'];
        }

        if (isset($data['dueDate'])) {
            $this->dueDate = new \DateTime($data['dueDate']);
        }

        if (isset($data['priority'])) {
            $this->priority = $data['priority'];
        }

        if (isset($data['done'])) {
            $this->done = (bool)$data['done'];
        }
    }

    /**
     * Validation fields
     */
    public function validate()
    {
        if (!in_array($this->priority, ['Low', 'Normal', 'High'])) {
            $this->validationErrors[] = 'Priority can be only Low, Normal or High';
        }

        if (strlen($this->title) > 256) {
            $this->validationErrors[] = 'Title can\'t be more than 256 characters';
        }
    }

    public function toArray():array
    {
        $result = [];

        if ($this->email) {
            $result['email'] = $this->email;
        }

        if ($this->title) {
            $result['title'] = $this->title;
        }

        if ($this->priority) {
            $result['priority'] = $this->priority;
        }

        if ($this->dueDate) {
            $result['dueDate'] = $this->dueDate->format('Y-m-d');
        }

        if ($this->done) {
            $result['done'] = $this->isDone();
        }

        return $result;
    }
}