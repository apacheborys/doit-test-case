<?php
namespace Entity;

class TokenEntity
{
    public const TOKEN_LENGTH = 16;

    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $token
     */
    private $token;

    /**
     * @var string[] $validationErrors
     */
    public $validationErrors = [];

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    public function fillFromArray($data)
    {
        if (isset($data['email'])) {
            $this->email = $data['email'];
        }

        if (isset($data['token'])) {
            $this->token = $data['token'];
        }
    }

    /**
     * Validation fields
     */
    public function validate()
    {
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";

        if (!preg_match($pattern, $this->email)) {
            $this->validationErrors[] = 'Email is wrong';
        }

        if (strlen($this->token) === self::TOKEN_LENGTH) {
            $this->validationErrors[] = 'TokenEntity length is not match';
        }

    }
}