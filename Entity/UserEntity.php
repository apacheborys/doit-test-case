<?php
namespace Entity;

class UserEntity
{
    /**
     * @var string $email
     */
    private $email;

    /**
     * @var string $password
     */
    private $password;

    /**
     * @var string[] $validationErrors
     */
    public $validationErrors = [];

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function fillFromArray($data)
    {
        if (isset($data['email'])) {
            $this->email = $data['email'];
        }

        if (isset($data['password'])) {
            $this->password = $data['password'];
        }
    }

    /**
     * Validation fields
     */
    public function validate()
    {
        $pattern = "/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/i";

        if (!preg_match($pattern, $this->email)) {
            $this->validationErrors[] = 'Email is wrong';
        }

        if (strlen($this->password) > 16) {
            $this->validationErrors[] = 'Password is too long. It must be less than 16 characters';
        }

        if (strlen($this->password) < 8) {
            $this->validationErrors[] = 'Password is too short. It must be more than 8 characters';
        }
    }
}