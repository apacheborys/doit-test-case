<?php
namespace Repository;

use Service\SqLiteDb;

abstract class AbstractRepository
{
    /**
     * @var SqLiteDb $db
     */
    public $db;

    public function __construct()
    {
        $this->db = new SqLiteDb();
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return $this->db->getLastError();
    }

    public function __destruct()
    {
        unset($this->db);
    }
}