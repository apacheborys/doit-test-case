<?php
namespace Repository;

use Entity\TaskEntity;

class TaskRepository extends AbstractRepository
{
    /**
     * @param TaskEntity $taskEntity
     * @return bool
     */
    public function insert(TaskEntity $taskEntity):bool
    {
        return $this->db->simpleQuery(
            "INSERT INTO `task` (`email`, `title`, `priority`, `dueDate`) VALUES ('"
            . $taskEntity->getEmail() . "', '" . $taskEntity->getTitle() . "', '" . $taskEntity->getPriority() . "', '"
            . $taskEntity->getDueDate()->format('Y-m-d') . "')"
        );
    }

    /**
     * @param TaskEntity $taskEntity
     * @return bool
     */
    public function update(TaskEntity $taskEntity):bool
    {
        return $this->db->simpleQuery(
            "UPDATE `task` SET `priority` = '" . $taskEntity->getPriority() . "', `dueDate` = '"
            . $taskEntity->getDueDate()->format("Y-m-d") . "', `done` = '" . (int)$taskEntity->isDone() . "' WHERE "
            ."`email` = '" . $taskEntity->getEmail() . "' AND `title` = '" . $taskEntity->getTitle() . "' LIMIT 1"
        );
    }
    
    /**
     * @param TaskEntity $taskEntity
     * @return bool
     */
    public function delete(TaskEntity $taskEntity):bool
    {
        return $this->db->simpleQuery(
            "DELETE FROM `task` WHERE `email` = '" . $taskEntity->getEmail() . "' AND `title` = '"
            . $taskEntity->getTitle() . "' LIMIT 1"
        );
    }

    /**
     * @param TaskEntity $taskEntity
     * @return bool
     */
    public function find(TaskEntity $taskEntity):bool
    {
        $result = $this->db->query(
            "SELECT COUNT(*) FROM `task` WHERE `email` = :email AND `title` = :title",
            [
                ':email' => $taskEntity->getEmail(),
                ':title' => $taskEntity->getTitle(),
            ]
        );

        $resultArray = $result->fetchArray(SQLITE3_ASSOC);

        if (!$resultArray || !isset($resultArray['COUNT(*)']) || $resultArray['COUNT(*)'] === 0) {
            return false;
        }

        return true;
    }

    /**
     * @param array $conditions
     * @param int $limit
     * @param int $offset
     * @param string $orderBy
     * @param string $orderDirection
     * @return TaskEntity[]
     */
    public function findBy(
        array $conditions,
        int $limit = 10,
        int $offset = 0,
        string $orderBy = 'title',
        string $orderDirection = 'ASC'
    ):array {
        $whereString = '';
        foreach (array_keys($conditions) as $fieldName) {
            if (strlen($whereString) > 0) {
                $whereString .= ' AND ';
            } else {
                $whereString = ' WHERE ';
            }
            $whereString .= '`' . $fieldName . '`' . ' = :' . $fieldName;
        }

        $argsForBind = [];
        foreach ($conditions as $fieldName => $condition) {
            $argsForBind[':' . $fieldName] = $condition;
        }

        $orderExpression = ' ORDER BY ' . $orderBy . ' ' . $orderDirection;

        $limitExpression = ' LIMIT ' . $limit;

        if ($offset > 0) {
            $limitExpression .= ' OFFSET ' . $offset;
        }

        $result = $this->db->query(
            "SELECT * FROM `task`" . $whereString . $orderExpression . $limitExpression,
            $argsForBind
        );

        $arrayForReturn = [];
        while($item = $result->fetchArray(SQLITE3_ASSOC)) {
            $tempTask = new TaskEntity();

            if (isset($item['title'])) {
                $tempTask->setTitle($item['title']);
            } else {
                continue;
            }

            if (isset($item['email'])) {
                $tempTask->setEmail($item['email']);
            } else {
                continue;
            }

            if (isset($item['priority'])) {
                $tempTask->setPriority($item['priority']);
            } else {
                continue;
            }

            if (isset($item['dueDate'])) {
                $tempTask->setDueDate(new \DateTime($item['dueDate']));
            } else {
                continue;
            }

            if (isset($item['done'])) {
                $tempTask->setDone((bool)$item['done']);
            } else {
                continue;
            }

            $tempTask->validate();

            if (count($tempTask->validationErrors) === 0) {
                $arrayForReturn[] = $tempTask;
            }
        }

        return $arrayForReturn;
    }
}