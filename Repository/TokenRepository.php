<?php
namespace Repository;

use Entity\TokenEntity;

class TokenRepository extends AbstractRepository
{
    /**
     * @param TokenEntity $tokenEntity
     * @return bool
     */
    public function insert(TokenEntity $tokenEntity):bool
    {
        return $this->db->simpleQuery(
            "INSERT INTO `token` (`email`, `token`) VALUES ('"
            . $tokenEntity->getEmail() . "', '" . md5($tokenEntity->getToken()) . "')"
        );
    }

    /**
     * @param TokenEntity $tokenEntity
     * @return bool
     */
    public function update(TokenEntity $tokenEntity):bool
    {
        return $this->db->simpleQuery(
            "UPDATE `token` SET `token` = '"
            . md5($tokenEntity->getToken()) . "' WHERE `email` = '" . $tokenEntity->getEmail() . "' LIMIT 1"
        );
    }
    
    /**
     * @param TokenEntity $tokenEntity
     * @return bool
     */
    public function delete(TokenEntity $tokenEntity):bool
    {
        return $this->db->simpleQuery(
            "DELETE FROM `token` WHERE `email` = '" . $tokenEntity->getEmail() . "' LIMIT 1"
        );
    }

    /**
     * @param TokenEntity $tokenEntity
     * @return bool
     */
    public function find(TokenEntity $tokenEntity):bool
    {
        $result = $this->db->query(
            "SELECT COUNT(*) FROM `token` WHERE `email` = :email AND `token` = :token",
            [
                ':email' => $tokenEntity->getEmail(),
                ':token' => md5($tokenEntity->getToken()),
            ]
        );

        $resultArray = $result->fetchArray(SQLITE3_ASSOC);

        if (!$resultArray || !isset($resultArray['COUNT(*)']) || $resultArray['COUNT(*)'] === 0) {
            return false;
        }

        return true;
    }

    /**
     * @param array $conditions
     * @return TokenEntity[]
     */
    public function findBy(array $conditions):array
    {
        $whereString = '';
        foreach (array_keys($conditions) as $fieldName) {
            if (strlen($whereString) > 0) {
                $whereString .= ' AND ';
            }
            $whereString .= '`' . $fieldName . '`' . ' = :' . $fieldName;
        }

        $argsForBind = [];
        foreach ($conditions as $fieldName => $condition) {
            $argsForBind[':' . $fieldName] = $condition;
        }

        $result = $this->db->query("SELECT * FROM `token` WHERE " . $whereString, $argsForBind);

        $arrayForReturn = [];
        while($item = $result->fetchArray(SQLITE3_ASSOC)) {
            $tempToken = new TokenEntity();

            if (isset($item['token'])) {
                $tempToken->setToken($item['token']);
            } else {
                continue;
            }

            if (isset($item['email'])) {
                $tempToken->setEmail($item['email']);
            } else {
                continue;
            }

            $tempToken->validate();

            if (count($tempToken->validationErrors) === 0) {
                $arrayForReturn[] = $tempToken;
            }
        }

        return $arrayForReturn;
    }
}