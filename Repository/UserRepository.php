<?php
namespace Repository;

use Entity\UserEntity;

class UserRepository extends AbstractRepository
{
    /**
     * @param UserEntity $userEntity
     * @return bool
     */
    public function insert(UserEntity $userEntity):bool
    {
        return $this->db->simpleQuery(
            "INSERT INTO `user` (`email`, `password`) VALUES ('"
            . $userEntity->getEmail() . "', '" . md5($userEntity->getPassword()) . "')"
        );
    }

    /**
     * @param UserEntity $userEntity
     * @return bool
     */
    public function update(UserEntity $userEntity):bool
    {
        return $this->db->simpleQuery(
            "UPDATE `user` SET `password` = '"
            . md5($userEntity->getPassword()) . "' WHERE `email` = '" . $userEntity->getEmail() . "' LIMIT 1"
        );
    }
    
    /**
     * @param UserEntity $userEntity
     * @return bool
     */
    public function delete(UserEntity $userEntity):bool
    {
        return $this->db->simpleQuery(
            "DELETE FROM `user` WHERE `email` = '" . $userEntity->getEmail() . "' LIMIT 1"
        );
    }

    /**
     * @param UserEntity $userEntity
     * @return bool
     */
    public function find(UserEntity $userEntity):bool
    {
        $result = $this->db->query(
            "SELECT COUNT(*) FROM `user` WHERE `email` = :email AND `password` = :password",
            [
                ':email' => $userEntity->getEmail(),
                ':password' => md5($userEntity->getPassword()),
            ]
        );

        $resultArray = $result->fetchArray(SQLITE3_ASSOC);

        if (!$resultArray || !isset($resultArray['COUNT(*)']) || $resultArray['COUNT(*)'] === 0) {
            return false;
        }

        return true;
    }
}