<?php
namespace Service;

class SqLiteDb
{
    /**
     * @var \SQLite3 $db
     */
    private $db;

    public function __construct()
    {
        $filename = implode(DIRECTORY_SEPARATOR , [__DIR__, '..', 'DB', 'dbSQLite3.db']);

        $this->db = new \SQLite3($filename);

        $fileDump = implode(DIRECTORY_SEPARATOR , [__DIR__, '..', 'DB', 'dump.sql']);

        if (is_file($fileDump)) {
            if (!$this->db->exec(file_get_contents($fileDump))) {
                echo 'Error happen during execution dump file - ' .
                    $this->db->lastErrorMsg() .
                    '. Code: ' .
                    $this->db->lastErrorCode();
            }
        }
    }

    /**
     * @param string $request
     * @param array $argsForBind
     * @return \SQLite3Result
     */
    public function query(string $request, array $argsForBind)
    {
        $st = $this->db->prepare($request);
        foreach ($argsForBind as $argName => $argValue)
        {
            $st->bindValue($argName, $argValue);
        }

        return $st->execute();
    }

    /**
     * @param string $request
     * @return bool
     */
    public function simpleQuery(string $request)
    {
        return $this->db->exec($request);
    }

    /**
     * @return string
     */
    public function getLastError()
    {
        return 'Message: ' . $this->db->lastErrorMsg() . '. Code: ' . $this->db->lastErrorCode();
    }

    public function __destruct()
    {
        $this->db->close();
    }
}