<?php
namespace Subscriber;

use Repository\TokenRepository;

class RequestSubscriber implements SubscriberInterface
{
    public function process()
    {
        if ($_SERVER['REQUEST_URI'] == '/sign_up' || $_SERVER['REQUEST_URI'] == '/sign_in') {
            return;
        }

        if (!isset($_SERVER['HTTP_AUTHORIZATION'])) {
            echo json_encode([
                'error' => 'Didn\'t find authorization header',
            ]);
            die();
        }

        $tokenRepo = new TokenRepository();
        $tokenFromDb = $tokenRepo->findBy(['token' => md5($_SERVER['HTTP_AUTHORIZATION'])]);

        if (!$tokenFromDb) {
            echo json_encode([
                'error' => 'Can\'t find token in db',
            ]);
            die();
        }

        putenv("USER=" . $tokenFromDb[0]->getEmail());
    }
}