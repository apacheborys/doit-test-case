<?php
namespace Subscriber;

interface SubscriberInterface
{
    /**
     * Process request
     *
     */
    public function process();
}