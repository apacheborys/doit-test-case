<?php

/**
 * Register autoload logic
 *
 * @param $classname
 */
function libraryRoot($classname) {
    $filename = __DIR__ . DIRECTORY_SEPARATOR . str_replace('\\',DIRECTORY_SEPARATOR, $classname) .".php";
    if (is_file($filename)) {
        require_once($filename);
    } else {
        echo 'Can\'t find file ' . $filename . ' for requiring class ' . $classname;
    }
}

spl_autoload_register('libraryRoot');

/**
 * Attempt to load http routes
 */
if (!is_file(__DIR__ . DIRECTORY_SEPARATOR . 'routes.php')) {
    echo 'Can\'t find routes map!';
    die();
}

include_once (__DIR__ . DIRECTORY_SEPARATOR . 'routes.php');

if (!isset($routes[ $_SERVER['REQUEST_URI'] . '@' . $_SERVER['REQUEST_METHOD'] ])) {
    echo 'Can\'t find action for this route';
    die();
}

/**
 * Process all subscribers what contain Subscriber directory
 */
if (is_dir(__DIR__ . DIRECTORY_SEPARATOR . 'Subscriber')) {
    foreach (scandir(__DIR__ . DIRECTORY_SEPARATOR . 'Subscriber') as $subscriber) {
        if ($subscriber === '..' || $subscriber === '.') {
            continue;
        }

        /**
         * Skip files what don't have ext
         */
        $extensionPosition = strrpos($subscriber, '.');
        $extension = substr($subscriber, $extensionPosition + 1);
        if ($extension !== 'php') {
            continue;
        }

        /**
         * Skip all files what don't have pattern <SubscriberName>Subscriber.php
         */
        if (strrpos($subscriber, 'Subscriber.php') + 14 !== strlen($subscriber)) {
            continue;
        }

        /**
         * @var \Subscriber\SubscriberInterface $objectSubscriber
         */
        $subscriberName = 'Subscriber\\' . substr($subscriber, 0, $extensionPosition);
        $objectSubscriber = new $subscriberName();

        $objectSubscriber->process();
    }
}

/**
 * Try to run method what specified in route map for received request url
 */

list($classname, $method) = explode(
    '::',
    $routes[ $_SERVER['REQUEST_URI'] . '@' . $_SERVER['REQUEST_METHOD'] ]
);

if (!class_exists($classname)) {
    echo 'Can\'t find class what indicates in route map';
    die();
}

$object = new $classname();

if (!method_exists($object, $method)) {
    echo 'Can\'t find method what indicates in route map';
    die();
}

$object->{$method}();

die();