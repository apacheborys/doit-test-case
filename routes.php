<?php
$routes = [
    '/sign_up@POST' => 'Controller\\UserController::signUpAction',
    '/sign_in@POST' => 'Controller\\UserController::signInAction',
    '/task@POST' => 'Controller\\TaskController::createTaskAction',
    '/task@PATCH' => 'Controller\\TaskController::updateTaskAction',
    '/task@DELETE' => 'Controller\\TaskController::deleteTaskAction',
    '/task@GET' => 'Controller\\TaskController::getTaskAction',
];